USE [BBDotNetDevTestDB]
GO

/****** Object:  Table [dbo].[Product]    Script Date: 02-06-2019 7:14:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product](
	[id] [bigint] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[quantity] [int] NOT NULL,
	[sale_amount] [money] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


