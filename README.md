

# BB Dot Net Developer Test - Solution

Solution for Web API Dot Net developer test. This solution built on **ASP.NET Core 2.2 MVC**, with tools like **EF (Entity Framework) Core** and **Microsoft.Extensions.Logging**. Also concept like **Dependency Injection** included. 

# Design/Architecture of the solution
 1. Solution is designed in 3 layers which are
	- Domain Layer: POCO classes
	- Service Layer: Interfaces and Services to handle specific requirement. Depending on Domain
	- Application Layer: Web API. Depending on Domain & Service.
 2. This Web API contain:
	- POST Method: api/Products/PutProducts.json
		- With Body Context as per request:
```
{
  "id": "(string) unique id of the event",
  "timestamp": "(timestamp) utc timestamp of the event",
  "products": [
    {
      "id": "(long) id of the product",
      "name": "(string) name of the product",
      "quantity": "(integer) quantity of the product",
      "sale_amount": "(double) total sale amount"
    }
  ]
}
```
    - GET Method: api/Products/GetProducts.json
      - With Body Context as per request

```
{
  "id": "(string) unique id of the event",
  "timestamp": "(timestamp) utc timestamp of the event",
  "products": [
    {
      "id": "(long) id of the product"
    }
  ]
}


```

# Step-by-step to setup the solution

1. Download or clone the source to a local directory.
2. Open the solution file with **Visual Studio 2017/2019(with .NET Core 2.2)**.
3. Create a Database in Microsoft SQL DB, in this case we used **[BBDotNetDevTestDB]** (Can use any but remember to change the #3 SQL script header).
4. Create a Tables by using the SQL Script Provided in the repository -**CreateProductTable.sql**. 
5. Insert Data to Authentication & Authorization Table

| username | password | enabled |
| --- | --- | --- |
| testuser | Abc@123 | False |
    
    
| username | function_code | enabled |
| --- | --- | --- |
| testuser | /api/Products/PutProducts.json | False |
| testuser | /api/Products/GetProducts.json | False |
    
6. Modify default connection string.
	- Open Project **BBDotNetDevTest.ProductAPI**
	- Open file **appsettings.json**
	- Modify **DefaultConnection** to your Database Connection String
    ![images/ModifiedAppsettingJson.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/ModifiedAppsettingJson.jpg)
7. Get the correct IP and Port for debug
	- Open **Properties** In Project **BBDotNetDevTest.ProductAPI**
	- Check On **Debug**
    
    ![mages/WebServerSettingForDebug.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/WebServerSettingForDebug.jpg)

8. Start Debug with **IISExpress**
![images/StartWithIISExpress.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/StartWithIISExpress.jpg)
9. Use POSTMAN to test the Web API
	- You download POSTMAN Application from the link [https://www.getpostman.com/products](https://www.getpostman.com/products)
	- You may have to turnoff SSL Certificate Verification if you are willing to test with SSL.
    ![/images/POSTMAN-TurnOffSSLVerification.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/POSTMAN-TurnOffSSLVerification.jpg)
    
    - First, we start from PutProducts. By using the request body text as below, we are expecting to insert 6 records to the Database.
    
	*{
  "id": "1234567890",
  "timestamp": "2019-05-31 10:20:00",
  "products": [
    {
      "id": "1",
      "name": "ABC T-Shirt",
      "quantity": "100",
      "sale_amount": "9.90"
    },
    {
      "id": "2",
      "name": "DEF T-Shirt",
      "quantity": "90",
      "sale_amount": "19.90"
    },
    {
      "id": "3",
      "name": "GHI T-Shirt",
      "quantity": "80",
      "sale_amount": "29.90"
    },
    {
      "id": "4",
      "name": "JKL T-Shirt",
      "quantity": "70",
      "sale_amount": "39.90"
    },
    {
      "id": "11",
      "name": "Blue Jeans",
      "quantity": "15",
      "sale_amount": "79.90"
    },
    {
      "id": "12",
      "name": "Black Jeans",
      "quantity": "25",
      "sale_amount": "119.90"
    }
  ]
}*

	- Remember to change Authorization to Basic Auth and key in correct username and password
    ![/images/POSTMAN-UseBasicAuth.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/9dd785db26be8a92267b217214db9996c7acf491/images/POSTMAN-UseBasicAuth.jpg)
    
    - Any failure of authorization will have a result return as 401 Unauthorized
    ![/images/POSTMAN-FailedBasicAuth.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/9dd785db26be8a92267b217214db9996c7acf491/images/POSTMAN-FailedBasicAuth.jpg)
    
    - Response with "errMsg": null, "innerErrMsg": null mean function completed with no error.	
    ![/images/UsingPOSTMANPost.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/UsingPOSTMANPost.jpg)
	
    - We may also double confirm the result by checking the Database	
    
    ![/images/CheckFromDatabase.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/CheckFromDatabase.jpg)
	
    - Follow with another test on GetProducts. By using the request body test as below, we are expecting return results of those 4 products.
    
	*{
  "id": "1234567890",
  "timestamp": "2019-05-31 10:20:00",
  "products": [
    {
      "id": "1"
    },
    {
      "id": "2"
    },
    {
      "id": "3"
    },
    {
      "id": "4"
    }
  ]
}*

![/images/UsingPOSTMANGet.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/UsingPOSTMANGet.jpg)
	
![/images/UsingPOSTMANGetResult.jpg](https://bitbucket.org/SengLiangPeng/bbdotnetdevtest/raw/79bee4a2e385b89510e8a2e8da4b0e4438f899d4/images/UsingPOSTMANGetResult.jpg)


## Other Test Cases

1. Send PutProducts.json request with duplicate product id.
2. Send GetProducts.json request with id that not in the DB.
