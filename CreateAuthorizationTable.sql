USE [BBDotNetDevTestDB]
GO

/****** Object:  Table [dbo].[Authorization]    Script Date: 07-06-2019 5:23:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Authorization](
	[username] [varchar](50) NOT NULL,
	[function_code] [varchar](255) NOT NULL,
	[enabled] [bit] NOT NULL,
 CONSTRAINT [PK_Authorization] PRIMARY KEY CLUSTERED 
(
	[username] ASC,
	[function_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


