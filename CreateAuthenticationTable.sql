USE [BBDotNetDevTestDB]
GO

/****** Object:  Table [dbo].[Authentication]    Script Date: 07-06-2019 5:23:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Authentication](
	[username] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[enabled] [bit] NOT NULL,
 CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


