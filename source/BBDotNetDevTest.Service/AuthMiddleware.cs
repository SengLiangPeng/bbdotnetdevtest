﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using BBDotNetDevTest.Domain;

namespace BBDotNetDevTest.Service
{   
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IAuthService service, ILogger<AuthMiddleware> logger)
        {
            try
            {
                string authHeader = context.Request.Headers["Authorization"];

                if (authHeader != null && authHeader.StartsWith("Basic"))
                {
                    //Extract credentials
                    string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                    int seperatorIndex = usernamePassword.IndexOf(':');

                    var username = usernamePassword.Substring(0, seperatorIndex);
                    var password = usernamePassword.Substring(seperatorIndex + 1);
                    
                    //Get user authorization
                    var auth = service.GetUserAuthorization(username, password, context.Request.Path);

                    if (auth)
                    {
                        await _next.Invoke(context);
                    }
                    else
                    {
                        context.Response.StatusCode = 401; //Unauthorized
                        return;
                    }
                }
                else
                {
                    // no authorization header
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.LogInformation(LoggingEvents.AuthMiddleware,
                    "[{dt}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));

                context.Response.StatusCode = 417; //Unauthorized
                return;
            }
        }
    }
}
