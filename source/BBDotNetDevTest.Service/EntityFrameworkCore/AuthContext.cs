﻿using BBDotNetDevTest.Domain.Account;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Service.EntityFrameworkCore
{
    /// <summary>
    /// DbContext for Products, Map with Database Table Product
    /// </summary>
    public class AuthContext : DbContext
    {
        public AuthContext(DbContextOptions<AuthContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //LP : Not using in current design as no orride needed
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //LP: Map Authentication class with Database Table Authentication
            modelBuilder.Entity<Authentication>().ToTable("Authentication");

            //LP: Map Authorization class with Database Table Authorization
            modelBuilder.Entity<Authorization>().ToTable("Authorization").HasKey(a => new
            {
                a.username,
                a.function_code
            });
        }

        //entities: 

        //LP: Authorizations
        public DbSet<Authorization> Authorizations { get; set; }
        //LP: Authentications
        public DbSet<Authentication> Authentications { get; set; }

    }
}
