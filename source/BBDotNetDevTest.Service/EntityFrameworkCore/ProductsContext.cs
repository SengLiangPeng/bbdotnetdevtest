﻿using BBDotNetDevTest.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Service.EntityFrameworkCore
{
    /// <summary>
    /// DbContext for Products, Map with Database Table Product
    /// </summary>
    public class ProductsContext : DbContext
    {
        public ProductsContext(DbContextOptions<ProductsContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //LP : Not using in current design as no orride needed
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Map Product class with Database Table Product
            modelBuilder.Entity<Product>().ToTable("Product");
        }

        //entities: Products
        public DbSet<Product> Products { get; set; }
    }
}
