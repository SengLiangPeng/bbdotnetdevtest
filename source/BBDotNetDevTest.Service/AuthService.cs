﻿using System;
using System.Linq;
using BBDotNetDevTest.Service.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using BBDotNetDevTest.Domain;

namespace BBDotNetDevTest.Service
{
    public class AuthService : IAuthService
    {
        private readonly AuthContext _context;
        private readonly ILogger _logger;

        public AuthContext Context { get { return _context; } }
        public ILogger Logger { get { return _logger; } }

        public AuthService(AuthContext context, ILogger<AuthService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public bool GetUserAuthorization(string username, string password, string code)
        {
            bool result = false;

            try
            {
                // Authentication - Check from DB by username & password
                var userauth = Context.Authentications.Where(ua => ua.username == username
                && ua.password == password
                && ua.enabled == true).FirstOrDefault();

                if (userauth != null)
                {
                    // Authorization - Check from DB by username & password
                    var functionauth = _context.Authorizations.Where(fa => fa.username == username
                            && fa.function_code == code
                            && fa.enabled == true).FirstOrDefault();

                    if (functionauth != null)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogInformation(LoggingEvents.AuthUser,
                    "[{dt}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            return result;
        }
    }
}
