﻿using BBDotNetDevTest.Domain;
using BBDotNetDevTest.Service.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace BBDotNetDevTest.Service
{
    public class ProductService : IProductService
    {
        private readonly ProductsContext _context;
        private readonly ILogger _logger;

        public ProductsContext Context { get { return _context; } }
        public ILogger Logger { get { return _logger; } }

        public ProductService(ProductsContext context, ILogger<ProductService> logger)
        {
            _context = context; // Products DB Context
            _logger = logger; // Logger
        }

        public ProductGetRes GetProducts(string jsonstr)
        {
            ProductGetRes _ProductGetRes = new ProductGetRes();

            try
            {          
                ProductGetReq _ProductGetReq = JsonConvert.DeserializeObject<ProductGetReq>(jsonstr);

                Logger.LogInformation(LoggingEvents.GetProducts,
                    "[{dt}][RequestID:{id}]\r\n[JsonBody:{json}]",
                    DateTime.Now,
                    _ProductGetReq.id,
                    jsonstr);

                //LP: Return request id
                _ProductGetRes.id = _ProductGetReq.id;

                //LP: Search by ID which matching with request products id
                _ProductGetRes.products = Context.Products.Where(
                    p => (_ProductGetReq.products.Any(rp => rp.id == p.id))).ToList();
            }
            catch (Exception ex)
            {
                //LP: Capture error message and inner error message to return to client
                _ProductGetRes.ErrMsg = ex.Message;
                _ProductGetRes.InnerErrMsg = ex.InnerException.Message;

                Logger.LogInformation(LoggingEvents.GetProducts,
                    "[{dt}][RequestID:{id}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    _ProductGetRes.id,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            return _ProductGetRes;
        }

        public ProductPutRes PutProducts(string jsonstr)
        {
            ProductPutRes _ProductPutRes = new ProductPutRes();

            try
            {
                ProductPutReq _ProductPutReq = new ProductPutReq();

                _ProductPutReq = JsonConvert.DeserializeObject<ProductPutReq>(jsonstr);

                Logger.LogInformation(LoggingEvents.PutProducts,
                    "[{dt}][RequestID:{id}]\r\n[JsonBody:{json}]",
                    DateTime.Now,
                    _ProductPutReq.id,
                    jsonstr);

                //LP: Return request id
                _ProductPutRes.id = _ProductPutReq.id;

                //LP: Add products to DBContext
                foreach (Product prod in _ProductPutReq.products)
                {
                    Context.Products.Add(prod);
                }

                //LP: Commited and save to DB
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                //LP: Capture error message and inner error message to return to client
                _ProductPutRes.ErrMsg = ex.Message;
                _ProductPutRes.InnerErrMsg = ex.InnerException.Message;

                Logger.LogInformation(LoggingEvents.PutProducts,
                    "[{dt}][RequestID:{id}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    _ProductPutRes.id,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            return _ProductPutRes;
        }

    }
}
