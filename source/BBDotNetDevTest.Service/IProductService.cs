﻿using BBDotNetDevTest.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Service
{
    public interface IProductService
    {
        ProductGetRes GetProducts(string jsonstr);

        ProductPutRes PutProducts(string jsonstr);
    }
}
