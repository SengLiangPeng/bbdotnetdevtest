﻿using BBDotNetDevTest.Domain.Account;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BBDotNetDevTest.Service
{
    public interface IAuthService
    {
        bool GetUserAuthorization(string username, string password, string code);
    }
}
