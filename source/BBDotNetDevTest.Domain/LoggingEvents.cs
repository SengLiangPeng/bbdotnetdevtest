﻿using Microsoft.Extensions.Logging;

namespace BBDotNetDevTest.Domain
{
    public static class LoggingEvents
    {
        public static EventId PutProducts = new EventId(1, "PP");
        public static EventId GetProducts = new EventId(2, "GP");
        public static EventId AuthUser = new EventId(3, "AU");
        public static EventId AuthMiddleware = new EventId(4, "AM");
    }
}
