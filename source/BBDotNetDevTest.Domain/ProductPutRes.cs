﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Domain
{
    /// <summary>
    /// Response Message for PutProducts
    /// ErrMsg: null if no error
    /// InnerErrMsg: null if no error
    /// Inherit: EventMsgBase
    /// </summary>
    public class ProductPutRes : EventMsgBase
    {
        public string ErrMsg { get; set; }
        public string InnerErrMsg { get; set; }
    }
}
