﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBDotNetDevTest.Domain.Account
{
    [Serializable]
    public class User
    {
        public Authentication Authentication { get; set; }
        public List<Authorization> Authorizations { get; set; }

        public bool GetAccessRight(string function_code)
        {
            var right = Authorizations.Where(n => n.function_code == function_code).FirstOrDefault();

            return (right == null ? false : right.enabled);
        }
    }
}
