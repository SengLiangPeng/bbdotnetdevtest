﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BBDotNetDevTest.Domain.Account
{
    [Serializable]
    public class Authentication
    {
        [Key]
        [Column(TypeName = "varchar(50)")]
        public string username { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string password { get; set; }
        public bool enabled { get; set; }
    }
}
