﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BBDotNetDevTest.Domain.Account
{
    [Serializable]
    public class Authorization
    {
        [Column(TypeName = "varchar(50)")]
        public string username { get; set; }

        [Column(TypeName = "varchar(5)")]
        public string function_code { get; set; }

        public bool enabled { get; set; }
    }
}
