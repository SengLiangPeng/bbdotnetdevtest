﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Domain
{    
    /// <summary>
     /// Request Message for GetProducts 
     /// Inherit: EventMsgBase
     /// products: Product List by request
     /// </summary>
    public class ProductGetReq : EventMsgBase
    {
        public List<Product> products { get; set; }
    }
}
