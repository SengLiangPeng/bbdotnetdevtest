﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBDotNetDevTest.Domain
{
    public class Product
    {
        [Key] //Primary Key
        public long id { get; set; }
                
        [StringLength(255)]
        [Column(TypeName = "nvarchar(255)")] //Mapping type of string to nvarchar(255)
        public string name { get; set; }

        public int quantity { get; set; }

        [Column(TypeName = "money")] //Mapping type of double to money
        public double sale_amount { get; set; }
    }
}
