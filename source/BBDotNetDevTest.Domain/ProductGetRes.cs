﻿using System;
using System.Collections.Generic;

namespace BBDotNetDevTest.Domain
{
    /// <summary>
    /// Response Message for GettProducts
    /// ErrMsg: null if no error
    /// InnerErrMsg: null if no error
    /// Inherit: EventMsgBase
    /// products: Product List for response
    /// </summary>
    public class ProductGetRes : EventMsgBase
    {
        public string ErrMsg { get; set; }
        public string InnerErrMsg { get; set; }
        public List<Product> products { get; set; }
    }
}
