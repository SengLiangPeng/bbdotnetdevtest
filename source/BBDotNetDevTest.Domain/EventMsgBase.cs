﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BBDotNetDevTest.Domain
{
    //Event messgae header, with unique id and timestamp
    public class EventMsgBase
    {
        [Key]
        [StringLength(20)]
        public string id { get; set; }

        [Required]
        public DateTime timestamp { get; set; }
    }
}
