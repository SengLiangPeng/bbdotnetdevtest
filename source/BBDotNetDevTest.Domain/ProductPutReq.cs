﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BBDotNetDevTest.Domain
{
    /// <summary>
    /// Request Message for PutProducts 
    /// Inherit: EventMsgBase
    /// products: Product List by request
    /// </summary>
    public class ProductPutReq : EventMsgBase
    {
        public List<Product> products { get; set; }
    }
}
