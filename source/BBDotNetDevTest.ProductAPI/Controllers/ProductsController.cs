﻿using System;
using Microsoft.AspNetCore.Mvc;
using BBDotNetDevTest.Domain;
using System.IO;
using System.Text;
using Microsoft.Extensions.Logging;
using BBDotNetDevTest.Service;

namespace BBDotNetDevTest.ProductAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly ILogger _logger;

        public IProductService Service { get { return _service; } }
        public ILogger Logger { get { return _logger; } }

        public ProductsController(IProductService service, ILogger<ProductsController> logger)
        {
            _service = service; //Product Service
            _logger = logger; // Logger
        }

        //GET: api/Products/GetProducts.json
        [HttpGet("GetProducts.json")]
        public ActionResult<ProductGetRes> GetProducts()
        {
            ProductGetRes _ProductGetRes = new ProductGetRes();

            try
            {                
                ProductGetReq _ProductPullReq = new ProductGetReq();

                string jsonstr = string.Empty;

                //LP: Getting GetProducts.json body context
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    jsonstr = reader.ReadToEnd();                    
                }

                _ProductGetRes = Service.GetProducts(jsonstr);
            }
            catch (Exception ex)
            {
                //LP: Capture error message and inner error message to return to client
                _ProductGetRes.ErrMsg = ex.Message;
                if (ex.InnerException != null) _ProductGetRes.InnerErrMsg = ex.InnerException.Message;

                Logger.LogInformation(LoggingEvents.GetProducts,
                    "[{dt}][RequestID:{id}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    _ProductGetRes.id,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            //LP: Return with timestamp which server completed job
            _ProductGetRes.timestamp = DateTime.Now;

            return _ProductGetRes;
        }

        #region PUT - Update Data - Not In Request
        // PUT: api/Products/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutProduct(long id, Product product)
        //{
        //    if (id != product.id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(product).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ProductExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}
        #endregion

        // POST: api/Products/PutProducts.json
        [HttpPost("PutProducts.json")]
        public ActionResult<ProductPutRes> PostProduct()
        {
            ProductPutRes _ProductPutRes = new ProductPutRes();

            try
            {
                string jsonstr = string.Empty;

                //LP: Getting GetProducts.json body context
                using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
                {
                    jsonstr = reader.ReadToEnd();
                }

                _ProductPutRes = Service.PutProducts(jsonstr);
            }
            catch (Exception ex)
            {
                //LP: Capture error message and inner error message to return to client
                _ProductPutRes.ErrMsg = "";

                Logger.LogInformation(LoggingEvents.GetProducts,
                    "[{dt}][RequestID:{id}] Err:{msg}\r\nInnerErr:{imsg}",
                    DateTime.Now,
                    _ProductPutRes.id,
                    ex.Message,
                    (ex.InnerException == null ? "" : ex.InnerException.Message));
            }

            //LP: Return with timestamp which server completed job
            _ProductPutRes.timestamp = DateTime.Now;

            Logger.LogInformation(LoggingEvents.GetProducts, 
                "[{dt}][RequestID:{id}] PutProducts Response Created", 
                DateTime.Now, 
                _ProductPutRes.id);

            return _ProductPutRes;
        }

        #region DELETE - Delete Data - Not In Request
        // DELETE: api/Products/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Product>> DeleteProduct(long id)
        //{
        //    var product = await _context.Products.FindAsync(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Products.Remove(product);
        //    await _context.SaveChangesAsync();

        //    return product;
        //}
        #endregion
    }
}
