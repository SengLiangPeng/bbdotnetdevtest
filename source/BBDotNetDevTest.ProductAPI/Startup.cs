﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using BBDotNetDevTest.Service;
using Microsoft.EntityFrameworkCore;

namespace BBDotNetDevTest.ProductAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSession();
            services.AddHttpContextAccessor();

            //LP: Dependency Injection for Authentication Service, use int [AuthMiddleware]
            services.AddScoped<IAuthService, AuthService>();

            //LP: Dependency Injection for Product Service, use in [ProductsController]
            services.AddScoped<IProductService, ProductService>();

            //LP: Add DbContext, with using [DefaultConnection] from [appsetting.json] (connection string), use in [ProductService]
            services.AddDbContext<BBDotNetDevTest.Service.EntityFrameworkCore.ProductsContext>(options => 
                                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //LP: Add DbContext, with using [DefaultConnection] from [appsetting.json] (connection string), use in [AuthService]
            services.AddDbContext<BBDotNetDevTest.Service.EntityFrameworkCore.AuthContext>(options =>
                                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //LP: Add Microsoft.Extensions.Logging for logging, use in [ProductsController]
            services.AddLogging(logging =>
            {
                logging.AddConfiguration(Configuration.GetSection("Logging"));
                logging.AddConsole();
                logging.AddDebug();
                logging.AddEventSourceLogger();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSession();
            app.UseMiddleware<AuthMiddleware>(); // Authenitcation Middleware
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
